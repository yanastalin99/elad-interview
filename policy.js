if (typeof(Elad) == 'undefined') {
    var Elad = Elad || {
        __namespace: true
    };
}

Elad.Metadata = Elad.Metadata || {
    __namespace: true
};

Elad.Metadata.Policy = {
    LogicalName: 'el_policy',
    DisplayName: 'Policy',
    Id: 'el_policyid',
    Attributes: {
        EndDate: 'el_enddate',
        StartDate: 'el_startdate',
        PolicyNumber: 'el_policynumber',
        Owner: 'ownerid',
        PolicyHolder: 'el_policyholder',
        CoverageType: 'el_coveragetype',
        License: 'el_license',
        Address: 'el_address'
    },
    OptionSets: {
        CoverageType: {
            Mandatory: 102910000,
            Comprehensive: 102910001,
            Structure: 102910002,
            Content: 102910003
        }
    }
}
if (typeof(Elad) == 'undefined') {
    var Elad = Elad || {
        __namespace: true
    };
}

Elad.Policy = Elad.Policy || {
    __namespace: true
};

Elad.Policy.Form = (function() {
    function onLoad(executionContext) {
        onCoverageTypeChange(executionContext);
        onStartDateChange(executionContext);
    }

    function onCoverageTypeChange(executionContext) {
        var formContext = executionContext.getFormContext();
        
        var coverageType = formContext.getAttribute(Elad.Metadata.Policy.Attributes.CoverageType).getValue();

        if (coverageType == Elad.Metadata.Policy.OptionSets.CoverageType.Mandatory ||
            coverageType == Elad.Metadata.Policy.OptionSets.CoverageType.Comprehensive) {
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.License).setRequiredLevel("required");
            formContext.getControl(Elad.Metadata.Policy.Attributes.License).setVisible(true);
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.Address).setRequiredLevel("none");
            formContext.getControl(Elad.Metadata.Policy.Attributes.Address).setVisible(false);
        }
        else if (coverageType == Elad.Metadata.Policy.OptionSets.CoverageType.Structure||
                 coverageType == Elad.Metadata.Policy.OptionSets.CoverageType.Content) {
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.License).setRequiredLevel("none");
            formContext.getControl(Elad.Metadata.Policy.Attributes.License).setVisible(false);       
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.Address).setRequiredLevel("required");     
            formContext.getControl(Elad.Metadata.Policy.Attributes.Address).setVisible(true);
        }
    }

    function onStartDateChange(executionContext) {
        var formContext = executionContext.getFormContext();
        
        var startDate = formContext.getAttribute(Elad.Metadata.Policy.Attributes.StartDate).getValue();
        if (startDate) {
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.EndDate).setRequiredLevel("required");
            formContext.getControl(Elad.Metadata.Policy.Attributes.EndDate).setDisabled(false);
        }
        else {
            formContext.getAttribute(Elad.Metadata.Policy.Attributes.EndDate).setRequiredLevel("none");
            formContext.getControl(Elad.Metadata.Policy.Attributes.EndDate).setDisabled(true);          
        }
    }

    function onEndDateChange(executionContext) {
        var formContext = executionContext.getFormContext();

        var policyId = formContext.data.entity.getId().replace('{', '').replace('}', '');

        var endDate = formContext.getAttribute(Elad.Metadata.Policy.Attributes.EndDate).getValue();
        
        var currentDate = new Date();
        if (endDate > currentDate) {
            var policyNumber = formContext.getAttribute(Elad.Metadata.Policy.Attributes.PolicyNumber).getValue();
            var owner = formContext.getAttribute(Elad.Metadata.Policy.Attributes.Owner).getValue();
            var contact = formContext.getAttribute(Elad.Metadata.Policy.Attributes.PolicyHolder).getValue();

            var quickViewControl = formContext.ui.quickForms.get("policyholder");
            var mobile = quickViewControl.getControl("mobilephone").getAttribute().getValue();

            currentDate.setDate(currentDate.getDate() + 5);
            
            var taskData =
            {
                "subject": "פוליסה מספר " + policyNumber,
                "scheduledend": currentDate,
                "ownerid@odata.bind": "/systemusers(" + owner[0].id.replace('{', '').replace('}', '') + ")",
                "description": "שלום, " + owner[0].name + ". הפוליסה עבור הלחות " + contact[0].name + " תסתיים בתאריך " + endDate.toLocaleDateString("heb-IL") + 
                "\nיש ליצור קשר עם הלקוח לטובת אפשרות לחידוש הפוליסה." + "\n שם איש הקשר: " + contact[0].name + "\n נייד איש הקשר: " + mobile,
                "regardingobjectid_el_policy@odata.bind": "/el_policies(" + policyId + ")"
            }

            Xrm.WebApi.online.createRecord("task", taskData).then(
                function success(result) {
                    console.log("Task created with ID: " + result.id);
                    formContext.getControl("Timeline").refresh();
                },
                function (error) {
                    console.log(error.message);
                }
            );
        }
    }

    return {
        OnLoad: onLoad,
        OnEndDateChange: onEndDateChange,
        OnCoverageTypeChange: onCoverageTypeChange,
        OnStartDateChange: onStartDateChange 
    }
}());